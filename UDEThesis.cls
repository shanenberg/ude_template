\usepackage{graphicx}
\usepackage{array}




\usepackage{times}
\usepackage{float} 
\usepackage{setspace} 
\usepackage{fancyhdr}
\usepackage{palatino}
\usepackage{fixltx2e}
\usepackage{refstyle}
\usepackage[table,x11names]{xcolor}
\usepackage{soul}
\usepackage[section]{placeins}

%
% Hier die eigenen Daten reinschreiben
%
\LoadClass[onecolumn]{article}

\usepackage[a4paper, tmargin=1in, bmargin=1.2in]{geometry}


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{UDEThesis}[2016/04/26 Thesis document class]

\renewcommand{\normalsize}{\fontsize{12}{14}\selectfont}

\def\whatIsIt{\def\@whatIsIt}
\def\matrikelnr{\def\@matrikelnr}
\def\supervisor{\def\@supervisor}
\def\reviewerFirst{\def\@reviewerFirst}
\def\reviewerSecond{\def\@reviewerSecond}
\def\locationDate{\def\@locationDate}
\def\major{\def\@major}
\def\UDEAbstractDE{\def\@UDEAbstractDE}
\def\UDEAbstractENG{\def\@UDEAbstractENG}


%\DeclareOption{10pt}{\def\@@ptsize{10pt}}
%\DeclareOption{11pt}{\def\@@ptsize{11pt}}
%\DeclareOption{12pt}{\def\@@ptsize{12pt}}


\newcommand\appendConfirmation {
\newpage
\thispagestyle{empty}
\section*{Eidesstattliche Erkl\"arung}

Hiermit versichere ich, dass ich die vorliegende Arbeit ohne Hilfe Dritter
und nur mit den angegebenen Quellen und Hilfsmitteln angefertigt habe.
Ich habe alle Stellen, die ich aus den Quellen w\"ortlich oder inhaltlich 
entnommen habe, als solche kenntlich gemacht. Diese Arbeit hat in gleicher
oder \"ahnlicher Form noch keiner Pr\"ufungsbeh\"orde vorgelegen. \\ \\ \\

\par

\noindent \begin{flushleft}
\@locationDate
\par\end{flushleft}
}



\renewcommand\maketitle {
%\begin{frontpage}
\thispagestyle{empty}

  \vskip 2em
  \begin{center}

\noindent %
\begin{tabular}{>{\raggedright}p{0.5\columnwidth}>{\raggedleft}p{0.5\columnwidth}}
\begin{tabular}{c}
\includegraphics[scale=0.6]{template/ude-logo}\tabularnewline
\end{tabular} & %
\begin{tabular}{c}
\includegraphics[scale=0.9]{template/csm_paluno_03_024ce763d3_8b0a45d04b}\tabularnewline
\end{tabular}\tabularnewline
\end{tabular}


    \vskip 2em

    {paluno \\ The Ruhr Institute for Software Technology \\ 
Institut f\"ur Informatik und Wirtschaftsinformatik \\
Universit\"at Duisburg-Essen
    \par}
    \vskip 3em
    {
	  {\Large \@whatIsIt} \\
      \Huge \textbf {\@title} \par}
    \vskip 3em
    {\large  \par}
    {\large
      \lineskip .5em
      %\begin{tabular}[t]{c}
        {\LARGE \@author} \\
    	\vskip 1em
        {Matrikelnummer: }{\@matrikelnr} \\
	    {Studiengang: } {\@major} \\
    	\vskip 1.2em
	\@locationDate
    \par}
  \end{center}

  \mbox{}
  \vfill
  \noindent
  {Betreuer: } {\@supervisor\\}
  \noindent
  {1. Gutachter: } {\@reviewerFirst\\}
  \noindent
  {2. Gutachter: } {\@reviewerSecond\\}
  \vskip 1em

%\end{frontpage}

  \newpage
  \lhead[]{}
  \chead{}
  \pagenumbering{roman}
  \thispagestyle{fancy}

  \newpage 
  \pagestyle{fancy}
  \pagenumbering{roman}

  \section*{Abstract}
  \@UDEAbstractENG


  {\let\clearpage\relax \section*{Zusammenfassung}}
  \@UDEAbstractDE


  \newpage 
  \tableofcontents

  \newpage 
  \listoffigures
  \addcontentsline{toc}{section}{\listfigurename}

  \newpage
  \listoftables
  \addcontentsline{toc}{section}{\listtablename}

  \newpage
  \printnomenclature
  \addcontentsline{toc}{section}{\nomname}
  
  \newpage
  \pagenumbering{arabic}  

 
}
\linespread{1.2} % Zeilenabstand



  \lhead{}
  \chead{}
  \rhead{\MakeUppercase\rightmark}
  \pagestyle{fancy}
  
\newcommand{\sectionbreak}{\clearpage}  
  
\renewcommand\section{  \clearpage
						\@startsection {section}{1} {\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\Large\bfseries}
								   }

  
 \renewcommand{\sectionmark}[1]{ \markright{#1}{}}




